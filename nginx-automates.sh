#!/bin/bash

wget -q -O - https://nginx.org/keys/nginx_signing.key | apt-key add - &&\
    echo "deb https://nginx.org/packages/mainline/ubuntu/ bionic nginx" | tee -a /etc/apt/sources.list.d/nginx-repo.list &&\
    echo "deb-src https://nginx.org/packages/mainline/ubuntu/ bionic nginx" | tee -a /etc/apt/sources.list.d/nginx-repo.list &&\
    apt-get update && apt-get install nginx -y
