#!/bin/bash

set -eo pipefail


NAME=${NAME:="jenkins"}

HTTP_PORT=${HTTP_PORT:-"8080"}

HTTP_HOST=${HTTP_HOST:-"localhost"}

JENKINS_HOME=${JENKINS_HOME:-"/var/lib/jenkins"}

JENKINS_INSTALL_DIR=${JENKINS_INSTALL_DIR:-"/usr/share/jenkins"}

JENKINS_PLUGINS_DIR=${JENKINS_PLUGINS_DIR:-"${JENKINS_HOME}/plugins"}

JENKINS_INIT_DIR="${JENKINS_HOME}/init.groovy.d"

JENKINS_WAR="${JENKINS_INSTALL_DIR}/jenkins.war"

JENKINS_PASS=${JENKINS_PASS:-$(openssl rand -base64 32)}

JENKINS_ADMIN=${JENKINS_ADMIN:-$NAME}

JENKINS_ARGS=${JENKINS_ARGS:-"--useJmx --httpPort=$HTTP_PORT --httpListenAddress=$HTTP_HOST"}

JENKINS_PROPS=${JENKINS_PROPS:-"-Djenkins.install.runSetupWizard=false"}

JENKINS_PLUGINS=${JENKINS_PLUGINS:-"git gitlab-plugin matrix-auth docker-plugin docker-workflow authorize-project configuration-as-code"}


# RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0;0m'

notice_console_out() {
    echo -e ${GREEN}${1}${NC}
}


create_user_if_not_exist() {
    local EXIST=$(cat /etc/passwd | grep jenkins)
    if [ ! $EXIST ]; then
	notice_console_out "User $NAME doesn't exists. Creating user $NAME"
	useradd jenkins -d $JENKINS_HOME -r -p $(openssl rand -base64 32)
    else
	notice_console_out "User $NAME exists. Use it instead"
    fi
}


download_jenkins_war_if_not_exist() {
    local WORK_DIR=$(pwd)
    if [ ! -f "$JENKINS_WAR" ]; then
	notice_console_out "Jenkins war file not found. Download a new one."
	curl -sSfL --retry 10 --retry-delay 1 http://mirrors.jenkins.io/war-stable/latest/jenkins.war -o "${JENKINS_INSTALL_DIR}/jenkins.war" &&\
	    curl -sSfL --retry 10 --retry-delay 1 http://mirrors.jenkins.io/war-stable/latest/jenkins.war.sha256 -o "${JENKINS_INSTALL_DIR}/jenkins.war.sha256" &&\
	    cd $JENKINS_INSTALL_DIR &&\
	    sha256sum -c "${JENKINS_INSTALL_DIR}/jenkins.war.sha256" &&\
	    cd $WORK_DIR
    else
	notice_console_out "Jenkins war file found. Use it instead"
    fi
}


add_jenkins_user_to_group_docker() {
    if hash docker; then
	notice_console_out "docker command found, try to check if docker group exists"
	if getent group docker; then
	    notice_console_out "docker group exist, try to check if $NAME has been added to docker group."

	    local IS_ADDED_TO_GROUP=$(groups jenkins | grep docker)

	    if [ -z "$IS_ADDED_TO_GROUP" ]; then
		notice_console_out "$NAME hasn't been added to group docker, adding $NAME to group docker."
		gpasswd -a jenkins docker
	    else
		notice_console_out "$NAME has been added to group docker. Do nothing."
	    fi

	else
	    notice_console_out "docker group not found. Please check if docker daemon using docker group."
	fi
    else
	notice_console_out "docker command not found, do nothing"
    fi
}


create_service_unit() {

    local SERVICE_UNIT_FILE="${JENKINS_HOME}/jenkins.service"
    if [ ! -f "$SERVICE_UNIT_FILE" ]; then
	notice_console_out "Service file not found. Generating one."
	
	echo "[Unit]"                                                                   | tee $SERVICE_UNIT_FILE 1>/dev/null
	echo "Description=SonarQube Service Unit."                                      | tee -a $SERVICE_UNIT_FILE 1>/dev/null
	echo ""                                                                         | tee -a $SERVICE_UNIT_FILE 1>/dev/null
	echo "[Service]"                                                                | tee -a $SERVICE_UNIT_FILE 1>/dev/null
	echo "Type=simple"                                                              | tee -a $SERVICE_UNIT_FILE 1>/dev/null
	echo "Restart=always"                                                           | tee -a $SERVICE_UNIT_FILE 1>/dev/null
	echo "RestartSec=5"                                                             | tee -a $SERVICE_UNIT_FILE 1>/dev/null
	echo "User=$NAME"                                                               | tee -a $SERVICE_UNIT_FILE 1>/dev/null
	echo "Environment=JENKINS_HOME='${JENKINS_HOME}'"                               | tee -a $SERVICE_UNIT_FILE 1>/dev/null
	echo "Environment=HOME='${JENKINS_HOME}'"                                       | tee -a $SERVICE_UNIT_FILE 1>/dev/null
	echo "Environment=JENKINS_ARGS='${JENKINS_ARGS}'"                               | tee -a $SERVICE_UNIT_FILE 1>/dev/null
	echo "Environment=JENKINS_PROPS='${JENKINS_PROPS}'"                             | tee -a $SERVICE_UNIT_FILE 1>/dev/null
	echo "Environment=JENKINS_ADMIN='${JENKINS_ADMIN}'"                             | tee -a $SERVICE_UNIT_FILE 1>/dev/null
	echo "Environment=JENKINS_PASS='${JENKINS_PASS}'"                               | tee -a $SERVICE_UNIT_FILE 1>/dev/null
	echo "TimeoutStopSec=30"                                                        | tee -a $SERVICE_UNIT_FILE 1>/dev/null
	echo "ExecStart=/usr/bin/java $JENKINS_PROPS -jar ${JENKINS_WAR} $JENKINS_ARGS" | tee -a $SERVICE_UNIT_FILE 1>/dev/null
	echo ""                                                                         | tee -a $SERVICE_UNIT_FILE 1>/dev/null
	echo "[Install]"                                                                | tee -a $SERVICE_UNIT_FILE 1>/dev/null
	echo "WantedBy=multi-user.target"                                               | tee -a $SERVICE_UNIT_FILE 1>/dev/null
    else
	notice_console_out "Service Unit File found. Do nothing"
    fi
}


linking_service_unit() {
    
    local SERVICE_UNIT_FILE="${JENKINS_HOME}/jenkins.service"

    if [ ! -f "/etc/systemd/system/jenkins.service" ]; then
	notice_console_out "Service Unit File not found in /etc/systemd/system/jenkins.service. Linking from $SERVICE_UNIT_FILE"
	ln -s $SERVICE_UNIT_FILE /etc/systemd/system/
	notice_console_out "Reload systemd daemon"
	systemctl daemon-reload
    else
	notice_console_out "Service Unit File linked. Do nothing."
    fi
}


install_deps_os() {
    notice_console_out "Installing Open JDK 11 Headless and unzip"
    apt-get install openjdk-11-jdk-headless unzip -y 1>/dev/null
}


install_jenkins_plugins() {
    notice_console_out "Installing Plugins $JENKINS_PLUGINS"
    JENKINS_UC="https://updates.jenkins.io" REF=$JENKINS_HOME ./install-plugins.sh $JENKINS_PLUGINS 1> /dev/null
}


generate_groovy_script_for_admin() {

    if [ ! -f "${JENKINS_INIT_DIR}/default-admin.groovy" ]; then
	notice_console_out "Generate default-admin.groovy on ${JENKINS_INIT_DIR}/default-admin.groovy"
	cat > "${JENKINS_INIT_DIR}/default-admin.groovy" <<EOF
import jenkins.model.Jenkins;
import hudson.security.HudsonPrivateSecurityRealm
import hudson.security.GlobalMatrixAuthorizationStrategy

def env = System.getenv()

def jenkins = Jenkins.get()
if(!(jenkins.getSecurityRealm() instanceof HudsonPrivateSecurityRealm))
    jenkins.setSecurityRealm(new HudsonPrivateSecurityRealm(false))

if(!(jenkins.getAuthorizationStrategy() instanceof GlobalMatrixAuthorizationStrategy))
    jenkins.setAuthorizationStrategy(new GlobalMatrixAuthorizationStrategy())

try {
    jenkins.getSecurityRealm().loadUserByUsername(env.JENKINS_USER);
} catch(error) {

    def user = jenkins.getSecurityRealm().createAccount(env.JENKINS_ADMIN, env.JENKINS_PASS)
    user.save()

    jenkins.getAuthorizationStrategy().add(Jenkins.ADMINISTER, env.JENKINS_ADMIN)
    
}

jenkins.save()
EOF
    fi
}


create_jenkins_dir_skeleton() {
    notice_console_out "Create Jenkins Directory Skeleton"
    mkdir -p $JENKINS_INIT_DIR $JENKINS_INSTALL_DIR
}


change_ownership_of_jenkins_dirs() {
    notice_console_out "Change Ownership of Jenkins Directory"
    chown -R $NAME:$NAME $JENKINS_INSTALL_DIR
    chown -R $NAME:$NAME $JENKINS_HOME
}


install_deps_os &&\
    create_user_if_not_exist &&\
    create_jenkins_dir_skeleton &&\
    download_jenkins_war_if_not_exist &&\
    install_jenkins_plugins &&\
    generate_groovy_script_for_admin &&\
    create_service_unit &&\
    linking_service_unit &&\
    add_jenkins_user_to_group_docker &&\
    change_ownership_of_jenkins_dirs &&\
    notice_console_out "$JENKINS_ADMIN:$JENKINS_PASS"
