#!/bin/bash

STRIPED_IP_PUB=$(curl -s -4 icanhazip.com | tr "." "-")
_ROOT_DOMAIN=${_ROOT_DOMAIN:-"sslip.io"}

_SERVER_NAME=${_SERVER_NAME:-"jenkins"}
_SERVER_HOST=${HTTP_HOST:-"localhost"}
_SERVER_PORT=${HTTP_PORT:-"8080"}
_SERVER_DOMAIN=${_TARGET_DOMAIN:-"${STRIPED_IP_PUB}.${_ROOT_DOMAIN}"}


cat > /etc/nginx/conf.d/default.conf <<EOF
upstream $_SERVER_NAME {
  keepalive 32;
  server ${_SERVER_HOST}:${_SERVER_PORT};
}


server {
  listen			80;
  server_name			${_SERVER_DOMAIN};
  ignore_invalid_headers	off;

  access_log  /var/log/nginx/$_SERVER_NAME.access.log;
  error_log  /var/log/nginx/$_SERVER_NAME.error.log;
  
  location / {
      sendfile off;
      proxy_pass         http://$_SERVER_NAME;
      proxy_redirect     default;
      proxy_http_version 1.1;

      proxy_set_header   Host              \$host;
      proxy_set_header   X-Real-IP         \$remote_addr;
      proxy_set_header   X-Forwarded-For   \$proxy_add_x_forwarded_for;
      proxy_set_header   X-Forwarded-Proto \$scheme;
      proxy_max_temp_file_size 0;

      #this is the maximum upload size
      client_max_body_size       10m;
      client_body_buffer_size    1k;

      proxy_connect_timeout      90;
      proxy_send_timeout         90;
      proxy_read_timeout         90;
      proxy_buffering            off;
      proxy_request_buffering    off; # Required for HTTP CLI commands in Jenkins > 2.54
      proxy_set_header Connection ""; # Clear for keepalive
  }


  error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }
}
EOF
