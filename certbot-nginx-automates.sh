#!/bin/bash

# need _ROOT_DOMAIN for register the server
# need _DOMAIN_TARGET if not set will create ip-public.$_ROOT_DOMAIN

set -eo pipefail

CERTBOT_PPA="ppa:certbot/certbot"


if ! hash curl; then
    echo "curl not found. Please make sure curl command available."
    exit 1
fi

if ! hash nginx; then
    echo "nginx not installed. Please make sure nginx installed."
    exit 1
fi

if ! hash certbot; then
    echo "Installing certbot and python-certbot-nginx"
    apt-get update &&\
	apt-get install software-properties-common -y &&\
	add-apt-repository universe -y &&\
	add-apt-repository $CERTBOT_PPA -y &&\
	apt-get update &&\
	apt-get install certbot python-certbot-nginx -y
fi


STRIPED_IP_PUB=$(curl -s -4 icanhazip.com | tr "." "-")
_ROOT_DOMAIN=${_ROOT_DOMAIN:-"sslip.io"}

CERT_DOMAIN=${_DOMAIN_TARGET:-"${STRIPED_IP_PUB}.${_ROOT_DOMAIN}"}

CERT_ADMIN_EMAIL=${_ADMIN_EMAIL:-"admin@${CERT_DOMAIN}"}


certbot -d $CERT_DOMAIN \
	--nginx \
	-n \
	-m $CERT_ADMIN_EMAIL \
	--no-eff-email \
	--agree-tos \
	--no-self-upgrade \
	--no-bootstrap \
	--keep-until-expiring \
	--expand &&\
    
    certbot enhance \
	    -d $CERT_DOMAIN \
	    --nginx \
	    -n \
	    --cert-name ${CERT_DOMAIN} \
	    --redirect \
	    --hsts
